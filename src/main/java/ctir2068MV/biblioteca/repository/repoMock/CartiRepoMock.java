package ctir2068MV.biblioteca.repository.repoMock;


import ctir2068MV.biblioteca.model.Carte;
import ctir2068MV.biblioteca.repository.repoInterfaces.CartiRepoInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepoMock implements CartiRepoInterface {

	private List<Carte> carti;
	
	public CartiRepoMock(){
		carti = new ArrayList<Carte>();}
	
	@Override
	public void adaugaCarte(Carte c) {
		carti.add(c);
	}

	@Override
	public List<Carte> cautaCarte(String ref) throws Exception{
		List<Carte> carti = getCarti(); //1
		List<Carte> cartiGasite = new ArrayList<>(); //2
		if(ref == null) { //3
			throw new Exception("Autor invalid"); //4
		}
		int i=0; //5
		while (i<carti.size()){ //6
			List<String> referenti = carti.get(i).getReferenti(); //7
			if (!referenti.isEmpty()) { //8
				boolean flag = hasAuthor(ref, carti.get(i).getReferenti()); //9
				if (flag) { //10
					cartiGasite.add(carti.get(i)); //11
				}
			}
			i++; //12
		}
		return cartiGasite; //13
	}

	private boolean hasAuthor(String ref, List<String> autori) {
		int j = 0;
		boolean flag = false;
		while(j<autori.size()){
			if(autori.get(j).toLowerCase().contains(ref.toLowerCase())){
				flag = true;
				break;
			}
			j++;
		}
		return flag;
	}


	@Override
	public List<Carte> getCarti() {
		return carti;
	}

	@Override
	public void modificaCarte(Carte nou, Carte vechi) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stergeCarte(Carte c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(String an) {
		List<Carte> lc = getCarti();
		List<Carte> lca = new ArrayList<Carte>();
		for(Carte c:lc){
			if(c.getAnAparitie().equals(an)){
				lca.add(c);
			}
		}
		
		Collections.sort(lca,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getTitlu().compareTo(b.getTitlu())==0){
					for(int i = 0; i < a.getReferenti().size(); i++){
						if(!a.getReferenti().get(i).equals(b.getReferenti().get(i)))
							return  a.getReferenti().get(i).compareTo(b.getReferenti().get(i));
					}
				}
				
				return a.getTitlu().compareTo(b.getTitlu());
			}
		
		});
		
		return lca;
	}

}
