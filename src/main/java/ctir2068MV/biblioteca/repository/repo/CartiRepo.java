package ctir2068MV.biblioteca.repository.repo;


import ctir2068MV.biblioteca.model.Carte;
import ctir2068MV.biblioteca.repository.repoInterfaces.CartiRepoInterface;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepo implements CartiRepoInterface {
	
	private String file = "src/main/cartiBD.dat";
	
	public CartiRepo(){
		URL location = CartiRepo.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
	}
	
	@Override
	public void adaugaCarte(Carte c) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(c.toString());
			bw.newLine();
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Carte> getCarti() {
		List<Carte> lc = new ArrayList<Carte>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				lc.add(Carte.getCarteFromString(line));
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return lc;
	}

	@Override
	public void modificaCarte(Carte nou, Carte vechi) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stergeCarte(Carte c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Carte> cautaCarte(String ref) throws Exception{
		List<Carte> carti = getCarti(); //1
		List<Carte> cartiGasite = new ArrayList<>(); //2
		if(ref == null) { //3
			throw new Exception("Autor invalid"); //4
		}
		int i=0; //5
		while (i<carti.size()){ //6
			List<String> referenti = carti.get(i).getReferenti(); //7
			if (!referenti.isEmpty()) { //8
				boolean flag = hasAuthor(ref, carti.get(i).getReferenti()); //9
				if (flag) { //10
					cartiGasite.add(carti.get(i)); //11
				}
			}
			i++; //12
		}
		return cartiGasite; //13
	}

	private boolean hasAuthor(String ref, List<String> autori) {
		int j = 0;
		boolean flag = false;
		while(j<autori.size()){
			if(autori.get(j).toLowerCase().contains(ref.toLowerCase())){
				flag = true;
				break;
			}
			j++;
		}
		return flag;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(String an) {
		List<Carte> lc = getCarti();
		List<Carte> lca = new ArrayList<Carte>();
		for(Carte c:lc){
			if(c.getAnAparitie().equals(an)){
				lca.add(c);
			}
		}

		Collections.sort(lca,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getTitlu().compareTo(b.getTitlu())==0){
					for(int i = 0; i < a.getReferenti().size(); i++){
						if(!a.getReferenti().get(i).equals(b.getReferenti().get(i)))
							return  a.getReferenti().get(i).compareTo(b.getReferenti().get(i));
					}
				}

				return a.getTitlu().compareTo(b.getTitlu());
			}

		});

		return lca;
	}

}
