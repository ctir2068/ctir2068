package ctir2068MV.biblioteca.control;

import ctir2068MV.biblioteca.model.Carte;
import ctir2068MV.biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class TopDown {

    BibliotecaCtrl bibliotecaCtrl;

    @Before
    public void setUp() throws Exception {
        bibliotecaCtrl = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Labis"));
        c.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c);

        Carte c1 = new Carte();
        c1.setTitlu("Carte");
        c1.setReferenti(asList("Blaga"));
        c1.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c1);

        Carte c2 = new Carte();
        c2.setTitlu("Armin");
        c2.setReferenti(asList("Eminescu"));
        c2.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c2);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void adaugaCarteTest1() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("1992");
        c.setTitlu("Winnetou");

        int marimeInitiala = bibliotecaCtrl.getCarti().size();
        bibliotecaCtrl.adaugaCarte(c);
        int marimeFinala = bibliotecaCtrl.getCarti().size();
        assertEquals(marimeFinala - 1, marimeInitiala);
    }

    @Test
    public void sortareCarti1() throws Exception {
        String anCautare = "1992";

        List<Carte> cartiRezultat = bibliotecaCtrl.getCartiOrdonateDinAnul(anCautare);

        assertEquals(cartiRezultat.size(), 3);
        assertEquals(cartiRezultat.get(0).getTitlu(), "Armin");
        assertEquals(cartiRezultat.get(1).getReferenti().get(0), "Blaga");
        assertEquals(cartiRezultat.get(2).getReferenti().get(0), "Labis");
    }

    @Test
    public void cautaAutorTest5() throws Exception {
        String ref = "Eminescu";

        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);

        assertEquals(cartiRezultat.size(), 1);
    }

    @Test(expected = Exception.class)
    public void adaugaCarteTest6() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("-1");
        c.setTitlu("Winnetou");

        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test
    public void topDownTestAdaugaCautare() throws Exception {
        Carte c2 = new Carte();
        c2.setTitlu("Luceafarul");
        c2.setReferenti(asList("Eminescu"));
        c2.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c2);

        String ref = "Eminescu";

        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);

        assertEquals(cartiRezultat.size(), 2);
    }

    @Test
    public void topDownTestAdaugaCautareSortare() throws Exception {
        Carte c2 = new Carte();
        c2.setTitlu("Luceafarul");
        c2.setReferenti(asList("Eminescu"));
        c2.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c2);

        Carte c1 = new Carte();
        c1.setTitlu("Lacrimile");
        c1.setReferenti(asList("Blaga"));
        c1.setAnAparitie("1980");
        bibliotecaCtrl.adaugaCarte(c1);

        String ref = "Eminescu";

        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);

        assertEquals(cartiRezultat.size(), 2);

        String anCautare = "1992";

        List<Carte> cartiRezultat2 = bibliotecaCtrl.getCartiOrdonateDinAnul(anCautare);

        assertEquals(cartiRezultat2.size(), 4);
        assertEquals(cartiRezultat2.get(0).getTitlu(), "Armin");
        assertEquals(cartiRezultat2.get(1).getReferenti().get(0), "Blaga");
        assertEquals(cartiRezultat2.get(2).getReferenti().get(0), "Labis");
        assertEquals(cartiRezultat2.get(3).getTitlu(), "Luceafarul");
    }


} 