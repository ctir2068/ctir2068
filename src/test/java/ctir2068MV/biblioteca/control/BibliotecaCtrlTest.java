package ctir2068MV.biblioteca.control;

import ctir2068MV.biblioteca.model.Carte;
import ctir2068MV.biblioteca.repository.repo.CartiRepo;
import ctir2068MV.biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class BibliotecaCtrlTest {

    BibliotecaCtrl bibliotecaCtrl;

    @Before
    public void setUp() throws Exception {
        bibliotecaCtrl = new BibliotecaCtrl(new CartiRepoMock());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void adaugaCarteTest1() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("1992");
        c.setTitlu("Winnetou");

        int marimeInitiala = bibliotecaCtrl.getCarti().size();
        bibliotecaCtrl.adaugaCarte(c);
        int marimeFinala = bibliotecaCtrl.getCarti().size();
        assertEquals(marimeFinala - 1, marimeInitiala);
    }

    @Test(expected = Exception.class)
    public void adaugaCarteTest2() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie(null);
        c.setTitlu("Winnetou");

        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test(expected = Exception.class)
    public void adaugaCarteTest3() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("doua mii");
        c.setTitlu("Winnetou");

        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test(expected = Exception.class)
    public void adaugaCarteTest4() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("1992");
        c.setTitlu("");

        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test
    public void adaugaCarteTest5() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("1992");
        c.setTitlu("M");

        int marimeInitiala = bibliotecaCtrl.getCarti().size();
        bibliotecaCtrl.adaugaCarte(c);
        int marimeFinala = bibliotecaCtrl.getCarti().size();
        assertEquals(marimeFinala - 1, marimeInitiala);
    }

    @Test(expected = Exception.class)
    public void adaugaCarteTest6() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("-1");
        c.setTitlu("Winnetou");

        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test
    public void adaugaCarteTest7() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("0");
        c.setTitlu("Winnetou");

        int marimeInitiala = bibliotecaCtrl.getCarti().size();
        bibliotecaCtrl.adaugaCarte(c);
        int marimeFinala = bibliotecaCtrl.getCarti().size();
        assertEquals(marimeFinala - 1, marimeInitiala);
    }

    @Test(expected = Exception.class)
    public void adaugaCarteTest8() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("1992");
        c.setTitlu("+Win-#");

        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test
    public void cautaAutorTest1() throws Exception {
        String ref = "Eminescu";
        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);
        assertTrue(cartiRezultat.isEmpty());
    }

    @Test(expected = Exception.class)
    public void cautaAutorTest2() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Eminescu"));
        c.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c);

        String ref = null;

        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);
        assertTrue(cartiRezultat.isEmpty());
    }

    @Test
    public void cautaAutorTest3() throws Exception {
        Carte c = new Carte();
        c.setTitlu("CarteDoi");
        c.setReferenti(new ArrayList<String>());
        c.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c);
        String ref = "Eminescu";

        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);

        assertTrue(cartiRezultat.isEmpty());
    }

    @Test
    public void cautaAutorTest4() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Labis"));
        c.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c);

        Carte c1 = new Carte();
        c1.setTitlu("CarteDoi");
        c1.setReferenti(asList("Blaga"));
        c1.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c1);

        String ref = "Eminescu";

        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);

        assertTrue(cartiRezultat.isEmpty());
    }

    @Test
    public void cautaAutorTest5() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Labis"));
        c.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c);

        Carte c1 = new Carte();
        c1.setTitlu("CarteDoi");
        c1.setReferenti(asList("Blaga"));
        c1.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c1);

        Carte c2 = new Carte();
        c2.setTitlu("CarteTrei");
        c2.setReferenti(asList("Eminescu"));
        c2.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c2);

        String ref = "Eminescu";

        List<Carte> cartiRezultat = bibliotecaCtrl.cautaCarte(ref);

        assertEquals(cartiRezultat.size(), 1);
    }

    @Test
    public void sortareCarti1() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Labis"));
        c.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c);

        Carte c1 = new Carte();
        c1.setTitlu("Carte");
        c1.setReferenti(asList("Blaga"));
        c1.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c1);

        Carte c2 = new Carte();
        c2.setTitlu("Armin");
        c2.setReferenti(asList("Eminescu"));
        c2.setAnAparitie("1992");
        bibliotecaCtrl.adaugaCarte(c2);

        String anCautare = "1992";

        List<Carte> cartiRezultat = bibliotecaCtrl.getCartiOrdonateDinAnul(anCautare);

        assertEquals(cartiRezultat.size(), 3);
        assertEquals(cartiRezultat.get(0).getTitlu(), "Armin");
        assertEquals(cartiRezultat.get(1).getReferenti().get(0), "Blaga");
        assertEquals(cartiRezultat.get(2).getReferenti().get(0), "Labis");
    }

    @Test(expected = Exception.class)
    public void sortareCarti2() throws Exception {
        bibliotecaCtrl.getCartiOrdonateDinAnul("o mie noua sute noua zeci si noua");
    }
}